# dbDistro Repository

<p>This is where dbDistro will receive updates and have custom made packages like the GameBoy Developer Kit Enviroment.</p> 

<p>I did not create GBDK or any of the applications contained in it. I just made it easier to setup.</p>


Append this code to your <code>/etc/pacman.conf</code>

```
[dbDistro-repo]
SigLevel = Never
Server = https://gitlab.com/dbdistro/package-repos/pacman-repo/-/raw/main/$arch
```

